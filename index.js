console.log("Hello World!")


// Prompt 2 numbers and perform different arithmetic
// operations based on the total of two numbers:

let x = parseInt(prompt("Provide a number"));
let y = parseInt(prompt("Provide another number"));
let sum = x + y;
let difference = x - y;
let product = x * y;
let quotient = x / y;


if (sum <= 9) {
    window.alert(`The sum of the two numbers is: ${sum}`);
    console.warn(`The sum is less than 9`)
} else if (sum <= 19) {
    window.alert(`The difference of the two numbers is: ${difference}`)
} else if (sum <= 29) {
    window.alert(`The product of the two numbers is: ${product}`)
} else if (sum >= 30) {
    window.alert(`The quotient of the two numbers is: ${quotient}`)
}

// Prompt user and age
let name1 = prompt("What is your name?")
let age1 = prompt("What is your age?")

if (name1 != null || age1 != null) {
    window.alert(`Hello ${name1}. Your age is ${age1}`);
} else if (name1 == null + undefined && age1 == null + undefined) {
    window.alert(`Are you a time traveler?`);
}

// Create function named isLegalAge (HOW?)
function isLegalAge(age1) {
    if (age1 >= 18) {
        window.alert(`You are of legal age.`)
    } else if (age1 < 17) {
        window.alert(`You are not allowed here`)
    }
}

// Switch statement
switch (age1) {
    case 18:
        window.alert("You are now allowed to party");
        break;
    case 21:
        window.alert("You are now part of the adult society");
        break;
    case 65:
        window.alert("We thank you for your contribution to society");
        break;
    default:
        window.alert("Are you sure you are not an alien?");
        break;

}
// try catch finally
function error(age1) {

    try {
        alert(isLegAge(age1))
    }

    catch (error) {

        console.warn(error.message)
    }

    finally {
        alert("Error error?")
    }
}

error(age1);


